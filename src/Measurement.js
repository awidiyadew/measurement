import Unit from './Unit';

/**
 * The action of measuring something.
 */
export default class Measurement {
  constructor(unit, value) {
    this._unit = unit;
    this._value = value;
  }

  equals(anotherValue) {
    if (!anotherValue) {
      return false;
    }

    if (!(anotherValue._unit instanceof Unit)) {
      return false;
    }

    if (this._isDifferentType(anotherValue)) {
      return false;
    }

    const DECIMAL_TOLERANCE = 0.003;
    const convertedValue = this._unit.convertTo(anotherValue._unit, this._value);
    return Math.abs(convertedValue - anotherValue._value) < DECIMAL_TOLERANCE;
  }

  _isDifferentType(otherMeasurement) {
    const someUnit = otherMeasurement._unit;
    return (!someUnit || !otherMeasurement._value ||
      this._unit.isDifferentType(someUnit));
  }
}
