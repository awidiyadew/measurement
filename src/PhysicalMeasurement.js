import Measurement from './Measurement';

/**
 * Used to measuring physical characteristics
 */
export default class PhysicalMeasurement extends Measurement {
  add(otherMeasurement) {
    if (this._unit.isDifferentType(otherMeasurement._unit)) {
      throw new Error('Error: Add two different measurement type is not supported.');
    }
    const leftValue = this._unit.convertTo(otherMeasurement._unit, this._value);
    const rightValue = otherMeasurement._value;
    return new Measurement(otherMeasurement._unit, leftValue + rightValue);
  }
}
