/**
 * A quantity chosen as a standard in terms of which other quantities may be expressed
 */
export default class Unit {
  constructor(type, conversionFactor, conversionShift = 0) {
    this._conversionFactor = conversionFactor;
    this._type = type;
    this._conversionShift = conversionShift;
  }

  isDifferentType(anotherUnit) {
    if (!anotherUnit) {
      return true;
    }

    return this._type !== anotherUnit._type;
  }

  convertTo(target, value) {
    return (this._conversionFactor / target._conversionFactor) *
      (value - this._conversionShift) + target._conversionShift;
  }
}

Unit.WEIGHT = {};
Unit.DISTANCE = {};
Unit.TEMPERATURE = {};

Unit.KILOMETER = new Unit(Unit.DISTANCE, 1000);
Unit.METER = new Unit(Unit.DISTANCE, 1);
Unit.CENTIMETER = new Unit(Unit.DISTANCE, 0.01);
Unit.MILLIMETER = new Unit(Unit.DISTANCE, 0.001);

Unit.FAHRENHEIT = new Unit(Unit.TEMPERATURE, 1 / 9, 32);
Unit.CELCIUS = new Unit(Unit.TEMPERATURE, 1 / 5, 0);
Unit.KELVIN = new Unit(Unit.TEMPERATURE, 1 / 5, 273.15);

Unit.KILOGRAM = new Unit(Unit.WEIGHT, 1000);
Unit.GRAM = new Unit(Unit.WEIGHT, 1);
