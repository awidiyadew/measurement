import { expect } from 'chai';
import PhysicalMeasurement from '../src/PhysicalMeasurement';
import Unit from '../src/Unit';

describe('PhysicalMeasurement', () => {
  describe('#add', () => {
    it('should return 2kilograms for 1kg + 1 kg', () => {
      const oneKilogram = new PhysicalMeasurement(Unit.KILOGRAM, 1);
      const addResult = oneKilogram.add(oneKilogram);
      const expectedResult = new PhysicalMeasurement(Unit.KILOGRAM, 2);
      expect(addResult.equals(expectedResult)).to.be.true;
    });

    it('should return 1001gr for 1kg + 1gr', () => {
      const oneKilogram = new PhysicalMeasurement(Unit.KILOGRAM, 1);
      const oneGram = new PhysicalMeasurement(Unit.GRAM, 1);
      const sumResult = oneKilogram.add(oneGram);
      const expectedMeasurement = new PhysicalMeasurement(Unit.GRAM, 1001);
      expect(sumResult.equals(expectedMeasurement)).to.be.true;
    });

    it('should throw error if input is 1kg and 1km', () => {
      const oneKilogram = new PhysicalMeasurement(Unit.KILOGRAM, 1);
      const oneKilometer = new PhysicalMeasurement(Unit.KILOMETER, 1);
      const badFn = () => {
        oneKilogram.add(oneKilometer);
      };
      expect(badFn).to.throw(Error);
    });

    it('should not throw error if input is 5kg and 5kg', () => {
      const oneKilogram = new PhysicalMeasurement(Unit.KILOGRAM, 5);
      const oneKilometer = new PhysicalMeasurement(Unit.KILOGRAM, 5);
      const goodFn = () => {
        oneKilogram.add(oneKilometer);
      };
      expect(goodFn).to.not.throw(Error);
    });
  });
});
