import { expect } from 'chai';
import Measurement from '../src/Measurement';
import Unit from '../src/Unit';

describe('Measurement', () => {
  describe('#equals', () => {
    it('true if input are 1 kilometer and 1 kilometer', () => {
      const kiloMeters = new Measurement(Unit.KILOMETER, 1);
      const thousandMeters = new Measurement(Unit.KILOMETER, 1);
      expect(kiloMeters.equals(thousandMeters)).to.be.true;
    });

    it('true if input are 1000 meters and 1 kilometer', () => {
      const kiloMeters = new Measurement(Unit.KILOMETER, 1);
      const thousandMeters = new Measurement(Unit.METER, 1000);
      expect(kiloMeters.equals(thousandMeters)).to.be.true;
    });

    it('true if input are 10 millimeter and 1 cm', () => {
      const oneCentimeters = new Measurement(Unit.CENTIMETER, 1);
      const tenMillimeter = new Measurement(Unit.MILLIMETER, 10);
      expect(oneCentimeters.equals(tenMillimeter)).to.be.true;
    });

    it('true if input are 1 centimeter and 10 millimeter', () => {
      const tenMillimeters = new Measurement(Unit.MILLIMETER, 10);
      const oneCentimeter = new Measurement(Unit.CENTIMETER, 1);
      expect(tenMillimeters.equals(oneCentimeter)).to.be.true;
    });

    it('false if input are 1 glok and 1 blah', () => {
      const oneBlah = new Measurement('blah', 1);
      const oneGlok = new Measurement('glok', 1);
      expect(oneBlah.equals(oneGlok)).to.be.false;
    });

    it('true if input are 1 gram and 1 gram', () => {
      const oneGrams = new Measurement(Unit.GRAM, 1);
      const otherOneGrams = new Measurement(Unit.GRAM, 1);
      expect(oneGrams.equals(otherOneGrams)).to.be.true;
    });

    it('false if input are 1 kg and 1 gram', () => {
      const oneKilogram = new Measurement(Unit.KILOGRAM, 1);
      const oneGram = new Measurement(Unit.GRAM, 1);
      expect(oneKilogram.equals(oneGram)).to.be.false;
    });

    it('false if input are 1 kilometer and 1000 kilogram', () => {
      const distance = new Measurement(Unit.KILOMETER, 1);
      const mass = new Measurement(null, 1000);
      expect(distance.equals(mass)).to.be.false;
    });

    it('false if input are 0.000343 kg and 0.343 gram', () => {
      const oneCentimeters = new Measurement(Unit.KILOGRAM, 0.000343);
      const tenMillimeter = new Measurement(Unit.GRAM, 0.343);
      expect(oneCentimeters.equals(tenMillimeter)).to.be.true;
    });

    it('false if inputs are 3kg and 3km', () => {
      const oneCentimeters = new Measurement(Unit.KILOGRAM, 1);
      const tenMillimeter = new Measurement(Unit.KILOMETER, 1);
      expect(oneCentimeters.equals(tenMillimeter)).to.be.false;
    });

    it('true if input are 1`F and 1`F', () => {
      const oneFahrenheit = new Measurement(Unit.FAHRENHEIT, 1);
      expect(oneFahrenheit.equals(oneFahrenheit)).to.be.true;
    });

    it('true if input are 0`C and 32`F', () => {
      const celcius = new Measurement(Unit.CELCIUS, 0);
      const fahrenheit = new Measurement(Unit.FAHRENHEIT, 32);
      expect(celcius.equals(fahrenheit)).to.be.true;
    });

    it('true if input are -40`C and -40`F', () => {
      const celcius = new Measurement(Unit.CELCIUS, -40);
      const fahrenheit = new Measurement(Unit.FAHRENHEIT, -40);
      expect(celcius.equals(fahrenheit)).to.be.true;
    });

    it('true if input are 0`C and 273`K', () => {
      const celcius = new Measurement(Unit.CELCIUS, 0);
      const kelvin = new Measurement(Unit.KELVIN, 273.15);
      expect(celcius.equals(kelvin)).to.be.true;
    });
  });
});
