import { expect } from 'chai';
import Unit from '../src/Unit';

describe('Unit', () => {
  describe('#convertTo', () => {
    it('returns 1000 for 1 km', () => {
      const kilometerUnit = new Unit(Unit.DISTANCE, 1000);
      const meterUnit = new Unit(Unit.DISTANCE, 1);
      const oneKilometer = 1;
      expect(kilometerUnit.convertTo(meterUnit, oneKilometer)).to.eq(1000);
    });

    it('returns 2000 for 2 km', () => {
      const kilometerUnit = new Unit(Unit.DISTANCE, 1000);
      const meterUnit = new Unit(Unit.DISTANCE, 1);
      const twoKilometers = 2;
      const twoThousandMeters = 2000;
      expect(kilometerUnit.convertTo(meterUnit, twoKilometers)).to.eq(twoThousandMeters);
    });
  });

  describe('#isDifferentType', () => {
    it('is true for km and km', () => {
      const kilometerUnit = new Unit(Unit.DISTANCE, 1000);
      const anotherKilometerUnit = new Unit(Unit.DISTANCE, 1000);
      expect(kilometerUnit.isDifferentType(anotherKilometerUnit)).to.be.false;
    });

    it('is false for km and m', () => {
      const kilometerUnit = new Unit(Unit.DISTANCE, 1000);
      const meterUnit = new Unit(Unit.DISTANCE, 1);
      expect(kilometerUnit.isDifferentType(meterUnit)).to.be.false;
    });

    it('is false for km and null', () => {
      const kilometerUnit = new Unit(Unit.DISTANCE, 1000);
      expect(kilometerUnit.isDifferentType(null)).to.be.true;
    });

    it('is false for km and kg', () => {
      const kilometerUnit = new Unit(Unit.DISTANCE, 1000);
      const kilogramUnit = new Unit(Unit.WEIGHT, 1000);
      expect(kilometerUnit.isDifferentType(kilogramUnit)).to.be.true;
    });
  });
});
